//------------------------------------------------------------------------------------------------------------------------------
// Samuel Williams
// SWWilliams@lbl.gov
// Lawrence Berkeley National Lab
//------------------------------------------------------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <math.h>

//------------------------------------------------------------------------------------------------------------------------------
#include "timer.h"
#include "defines.h"
#include "box.h"
#include "mg.h"
#include "operators.h"
//------------------------------------------------------------------------------------------------------------------------------
#include "operators.naive/buffer_copy.c"
#include "operators.reference/ghosts.c"
#include "operators.ompif/exchange_boundary.c"
#include "operators.naive/lampda.c"
#include "operators.reference/gsrb.c"
#include "operators.reference/residual.c"
#include "operators.naive/restriction.c"
#include "operators.naive/interpolation.c"
#include "operators.naive/misc.c"
//------------------------------------------------------------------------------------------------------------------------------
