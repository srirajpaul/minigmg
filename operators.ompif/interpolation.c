//------------------------------------------------------------------------------------------------------------------------------
// Samuel Williams
// SWWilliams@lbl.gov
// Lawrence Berkeley National Lab
//------------------------------------------------------------------------------------------------------------------------------
#include <stdint.h>
#include "../timer.h"
//------------------------------------------------------------------------------------------------------------------------------
// piecewise constant interpolation...
//
// +-------+    +---+---+
// |       |    | x | x |
// |   x   | -> +---+---+
// |       |    | x | x |
// +-------+    +---+---+
//
//------------------------------------------------------------------------------------------------------------------------------
void interpolation_constant(domain_type * domain, int level_f, double prescale_f, int id_f, int id_c){ // id_f = prescale*id_f + I_{2h}^{h}(id_c)
  int level_c = level_f+1;
  uint64_t _timeStart = CycleTime();

  int CollaborativeThreadingBoxSize = 100000; // i.e. never
  #ifdef __COLLABORATIVE_THREADING
    CollaborativeThreadingBoxSize = 1 << __COLLABORATIVE_THREADING;
  #endif
  int omp_across_boxes = (domain->subdomains[0].levels[level_f].dim.i <  CollaborativeThreadingBoxSize);
  int omp_within_a_box = (domain->subdomains[0].levels[level_f].dim.i >= CollaborativeThreadingBoxSize);
  int box;

  #pragma omp parallel for private(box) if(omp_across_boxes)
  for(box=0;box<domain->subdomains_per_rank;box++){
    int i,j,k;
    int ghosts_c = domain->subdomains[box].levels[level_c].ghosts;
    int pencil_c = domain->subdomains[box].levels[level_c].pencil;
    int  plane_c = domain->subdomains[box].levels[level_c].plane;
  
    int ghosts_f = domain->subdomains[box].levels[level_f].ghosts;
    int pencil_f = domain->subdomains[box].levels[level_f].pencil;
    int  plane_f = domain->subdomains[box].levels[level_f].plane;
    int  dim_i_f = domain->subdomains[box].levels[level_f].dim.i;
    int  dim_j_f = domain->subdomains[box].levels[level_f].dim.j;
    int  dim_k_f = domain->subdomains[box].levels[level_f].dim.k;
  
    double * __restrict__ grid_f = domain->subdomains[box].levels[level_f].grids[id_f] + ghosts_f*(1+pencil_f+plane_f);
    double * __restrict__ grid_c = domain->subdomains[box].levels[level_c].grids[id_c] + ghosts_c*(1+pencil_c+plane_c);
  
    #pragma omp parallel for private(k,j,i) if(omp_within_a_box) collapse(2)
    for(k=0;k<dim_k_f;k++){
    for(j=0;j<dim_j_f;j++){
    for(i=0;i<dim_i_f;i++){
      int ijk_f = (i   ) + (j   )*pencil_f + (k   )*plane_f;
      int ijk_c = (i>>1) + (j>>1)*pencil_c + (k>>1)*plane_c;
      grid_f[ijk_f] = prescale_f*grid_f[ijk_f] + grid_c[ijk_c];
    }}}
  }
  domain->cycles.interpolation[level_f] += (uint64_t)(CycleTime()-_timeStart);
}

//------------------------------------------------------------------------------------------------------------------------------
// piecewise linear interpolation...
//
//------------------------------------------------------------------------------------------------------------------------------
void interpolation_linear(domain_type * domain, int level_f, double prescale_f, int id_f, int id_c){  // id_f = prescale*id_f + I_{2h}^{h}(id_c)
  int level_c = level_f+1;
  uint64_t _timeStart = CycleTime();

  int CollaborativeThreadingBoxSize = 100000; // i.e. never
  #ifdef __COLLABORATIVE_THREADING
    CollaborativeThreadingBoxSize = 1 << __COLLABORATIVE_THREADING;
  #endif
  int omp_across_boxes = (domain->subdomains[0].levels[level_f].dim.i <  CollaborativeThreadingBoxSize);
  int omp_within_a_box = (domain->subdomains[0].levels[level_f].dim.i >= CollaborativeThreadingBoxSize);
  int box;

  #pragma omp parallel for private(box) if(omp_across_boxes)
  for(box=0;box<domain->subdomains_per_rank;box++){
    int i,j,k;
    int ghosts_c = domain->subdomains[box].levels[level_c].ghosts;
    int pencil_c = domain->subdomains[box].levels[level_c].pencil;
    int  plane_c = domain->subdomains[box].levels[level_c].plane;
    int  dim_i_c = domain->subdomains[box].levels[level_c].dim.i;
    int  dim_j_c = domain->subdomains[box].levels[level_c].dim.j;
    int  dim_k_c = domain->subdomains[box].levels[level_c].dim.k;

    int ghosts_f = domain->subdomains[box].levels[level_f].ghosts;
    int pencil_f = domain->subdomains[box].levels[level_f].pencil;
    int  plane_f = domain->subdomains[box].levels[level_f].plane;
    int  dim_i_f = domain->subdomains[box].levels[level_f].dim.i;
    int  dim_j_f = domain->subdomains[box].levels[level_f].dim.j;
    int  dim_k_f = domain->subdomains[box].levels[level_f].dim.k;

    double * __restrict__ grid_f = domain->subdomains[box].levels[level_f].grids[  id_f] + ghosts_f*(1 + pencil_f + plane_f); // [0] is first non-ghost zone element
    double * __restrict__ grid_c = domain->subdomains[box].levels[level_c].grids[  id_c] + ghosts_c*(1 + pencil_c + plane_c);

    // FIX what about dirichlet boundary conditions ???
    #pragma omp parallel for private(k,j,i) if(omp_within_a_box) collapse(2)
    for(k=0;k<dim_k_f;k++){
    for(j=0;j<dim_j_f;j++){
    for(i=0;i<dim_i_f;i++){
      int ijk_f = (i   ) + (j   )*pencil_f + (k   )*plane_f;
      int ijk_c = (i>>1) + (j>>1)*pencil_c + (k>>1)*plane_c;
      int delta_i=       -1;if(i&0x1)delta_i=       1; // i.e. even points look backwards while odd points look forward
      int delta_j=-pencil_c;if(j&0x1)delta_j=pencil_c;
      int delta_k= -plane_c;if(k&0x1)delta_k= plane_c;
      grid_f[ijk_f] =
        prescale_f*grid_f[ijk_f                        ] +
          0.421875*grid_c[ijk_c                        ] +
          0.140625*grid_c[ijk_c                +delta_k] +
          0.140625*grid_c[ijk_c        +delta_j        ] +
          0.046875*grid_c[ijk_c        +delta_j+delta_k] +
          0.140625*grid_c[ijk_c+delta_i                ] +
          0.046875*grid_c[ijk_c+delta_i        +delta_k] +
          0.046875*grid_c[ijk_c+delta_i+delta_j        ] +
          0.015625*grid_c[ijk_c+delta_i+delta_j+delta_k] ;
    }}}
  }
  domain->cycles.interpolation[level_f] += (uint64_t)(CycleTime()-_timeStart);
}
//------------------------------------------------------------------------------------------------------------------------------
