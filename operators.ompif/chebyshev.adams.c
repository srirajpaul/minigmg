//------------------------------------------------------------------------------------------------------------------------------
// Samuel Williams
// SWWilliams@lbl.gov
// Lawrence Berkeley National Lab
//------------------------------------------------------------------------------------------------------------------------------
#include <stdint.h>
#include "../timer.h"
//------------------------------------------------------------------------------------------------------------------------------
// Based on Mark Adam's Prometheus Code...
//------------------------------------------------------------------------------------------------------------------------------
#define DEGREE 4
void smooth(domain_type * domain, int level, int x_id, int rhs_id, double a, double b, int sweep){
  uint64_t _timeStart = CycleTime();

  int CollaborativeThreadingBoxSize = 100000; // i.e. never
  #ifdef __COLLABORATIVE_THREADING
    CollaborativeThreadingBoxSize = 1 << __COLLABORATIVE_THREADING;
  #endif
  int omp_across_boxes = (domain->subdomains[0].levels[level].dim.i <  CollaborativeThreadingBoxSize);
  int omp_within_a_box = (domain->subdomains[0].levels[level].dim.i >= CollaborativeThreadingBoxSize);
  int box;

  if( (domain->dominant_eigenvalue_of_DinvA[level]<=0.0) && (domain->rank==0) )printf("dominant_eigenvalue_of_DinvA[%d] <= 0.0 !\n",level);
  double beta     = 1.100*domain->dominant_eigenvalue_of_DinvA[level]; // lambda_max may have been an underestimate
  double alpha    = 0.125*beta;
  double theta    = 0.5*(beta+alpha); // center of the spectral ellipse
  double delta    = 0.5*(beta-alpha); // major axis?
  double sigma = theta/delta;
  double rho_k = 1/sigma; // rho_0

  double chebyshev_c1[16]; // scales Dk
  double chebyshev_c2[16]; // scales lambda*(b-Ax)
  int s;
  chebyshev_c1[0] = 0.0;
  chebyshev_c2[0] = 1.0/theta;
  for(s=1;s<16;s++){
    double rho_km1 = rho_k;
    rho_k = 1.0/(2.0*sigma - rho_km1);
    chebyshev_c1[s] =     rho_k*rho_km1;
    chebyshev_c2[s] = 2.0*rho_k/delta;
  }


  #pragma omp parallel for private(box,s) if(omp_across_boxes)
  for(box=0;box<domain->subdomains_per_rank;box++){
    int i,j,k,s;
    int pencil = domain->subdomains[box].levels[level].pencil;
    int  plane = domain->subdomains[box].levels[level].plane;
    int ghosts = domain->subdomains[box].levels[level].ghosts;
    int  dim_k = domain->subdomains[box].levels[level].dim.k;
    int  dim_j = domain->subdomains[box].levels[level].dim.j;
    int  dim_i = domain->subdomains[box].levels[level].dim.i;
    double h2inv = 1.0/(domain->h[level]*domain->h[level]);
    double * __restrict__ x         = domain->subdomains[box].levels[level].grids[    x_id] + ghosts*(1+pencil+plane); // i.e. [0] = first non ghost zone point
    double * __restrict__ Dk        = domain->subdomains[box].levels[level].grids[__temp  ] + ghosts*(1+pencil+plane); 
    double * __restrict__ rhs       = domain->subdomains[box].levels[level].grids[  rhs_id] + ghosts*(1+pencil+plane);
    double * __restrict__ alpha     = domain->subdomains[box].levels[level].grids[__alpha ] + ghosts*(1+pencil+plane);
    double * __restrict__ beta_i    = domain->subdomains[box].levels[level].grids[__beta_i] + ghosts*(1+pencil+plane);
    double * __restrict__ beta_j    = domain->subdomains[box].levels[level].grids[__beta_j] + ghosts*(1+pencil+plane);
    double * __restrict__ beta_k    = domain->subdomains[box].levels[level].grids[__beta_k] + ghosts*(1+pencil+plane);
    double * __restrict__ lambda    = domain->subdomains[box].levels[level].grids[__lambda] + ghosts*(1+pencil+plane);

    int ghostsToOperateOn=ghosts-1;
    for(s=0;s<ghosts;s++,ghostsToOperateOn--){
      double c1 = chebyshev_c1[(sweep+s)%DEGREE]; // limit polynomial to degree DEGREE.
      double c2 = chebyshev_c2[(sweep+s)%DEGREE]; // limit polynomial to degree DEGREE.
      #pragma omp parallel for private(k,j,i) if(omp_within_a_box) collapse(2)
      for(k=0-ghostsToOperateOn;k<dim_k+ghostsToOperateOn;k++){
      for(j=0-ghostsToOperateOn;j<dim_j+ghostsToOperateOn;j++){
      for(i=0-ghostsToOperateOn;i<dim_i+ghostsToOperateOn;i++){
        int ijk = i + j*pencil + k*plane;
        double Ax =  a*alpha[ijk]*x[ijk]
                         -b*h2inv*(
                            beta_i[ijk+1     ]*( x[ijk+1     ]-x[ijk       ] )
                           -beta_i[ijk       ]*( x[ijk       ]-x[ijk-1     ] )
                           +beta_j[ijk+pencil]*( x[ijk+pencil]-x[ijk       ] )
                           -beta_j[ijk       ]*( x[ijk       ]-x[ijk-pencil] )
                           +beta_k[ijk+plane ]*( x[ijk+plane ]-x[ijk       ] )
                           -beta_k[ijk       ]*( x[ijk       ]-x[ijk-plane ] )
                          );
        Dk[ijk] = c1*Dk[ijk] + c2*lambda[ijk]*(rhs[ijk]-Ax);
/*
        double lambda = 1.0 / ( a*alpha[ijk] +
                                b*h2inv*( beta_i[ijk] + beta_i[ijk+     1] +
                                          beta_j[ijk] + beta_j[ijk+pencil] +
                                          beta_k[ijk] + beta_k[ijk+ plane] )
                              );
        Dk[ijk] = c1*Dk[ijk] + c2*lambda*(rhs[ijk]-Ax);
*/
      }}}
      #pragma omp parallel for private(k,j,i) if(omp_within_a_box) collapse(2)
      for(k=0-ghostsToOperateOn;k<dim_k+ghostsToOperateOn;k++){
      for(j=0-ghostsToOperateOn;j<dim_j+ghostsToOperateOn;j++){
      for(i=0-ghostsToOperateOn;i<dim_i+ghostsToOperateOn;i++){
        int ijk = i + j*pencil + k*plane;
        x[ijk] = x[ijk] + Dk[ijk];
      }}}
    }
  }
  domain->cycles.smooth[level] += (uint64_t)(CycleTime()-_timeStart);
}

//------------------------------------------------------------------------------------------------------------------------------
