//------------------------------------------------------------------------------------------------------------------------------
// Samuel Williams
// SWWilliams@lbl.gov
// Lawrence Berkeley National Lab
//------------------------------------------------------------------------------------------------------------------------------
// Helmholtz ~ Laplacian() = a*alpha*Identity - b*Divergence*beta*Gradient
//      GSRB needs __lambda
//    Jacobi needs __lambda and __temp
// Chebyshev needs __temp
//------------------------------------------------------------------------------------------------------------------------------
#define  __u           0 // = what we're eventually solving for (u), cell centered
#define  __f           1 // = original right-hand side (Au=f), cell centered
#define  __alpha       2 // cell centered constant
#define  __beta_i      3 // face constant (n.b. element 0 is the left face of the ghost zone element)
#define  __beta_j      4 // face constant (n.b. element 0 is the back face of the ghost zone element)
#define  __beta_k      5 // face constant (n.b. element 0 is the bottom face of the ghost zone element)
#define  __lambda      6 // cell centered constant
#define  __ee          7 // = used for correction (ee) in residual correction form, cell centered
#define  __f_minus_Av  8 // = used for initial right-hand side (f-Av) in residual correction form, cell centered
#define  __temp        9 // = used for unrestricted residual (r), cell centered
//------------------------------------------------------------------------------------------------------------------
#define  __NumGrids   10 // total number of grids and the starting location for bottom solver grids
//------------------------------------------------------------------------------------------------------------------

// For CG bottom solver
#define  __r          11
#define  __p          12
#define  __Ap         14

// For BiCGStab bottom solver
#define  __r0         10
#define  __r          11
#define  __p          12
#define  __s          13
#define  __Ap         14
#define  __As         15

// For CABiCGStab bottom solver
#define  __rt         10
#define  __r          11
#define  __p          12
#define  __PRrt       13 // starting location for the 2S+1 extra p[]'s, 2S extra r[]'s, and rt

// For CACG bottom solver
#define  __r0         10
#define  __r          11
#define  __p          12
#define  __PRrt       13 // starting location for the S+1 extra p[]'s, S extra r[]'s, and rt



//------------------------------------------------------------------------------------------------------------------------------
// box[j].ghost[i] = box[box[j].neighbor[i]].surface[26-i]
//------------------------------------------------------------------------------------------------------------------------------
