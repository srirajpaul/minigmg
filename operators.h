//------------------------------------------------------------------------------------------------------------------------------
// Samuel Williams
// SWWilliams@lbl.gov
// Lawrence Berkeley National Lab
//------------------------------------------------------------------------------------------------------------------------------
  void                  apply_op(domain_type *domain, int level, int Ax_id,  int x_id,   double a, double b, int multiplyByDinv);
  void                  residual(domain_type *domain, int level, int res_id, int phi_id, int rhs_id, double a, double b);
  void                    smooth(domain_type *domain, int level, int phi_id, int rhs_id, double a, double b, int sweep);
  void            rebuild_lambda(domain_type *domain, int level, double a, double b);

  void  residual_and_restriction(domain_type *domain, int level_f, int phi_id, int rhs_id, int level_c, int res_id, double a, double b);
  void               restriction(domain_type *domain, int level_f, int id_c, int id_f);
  void         restriction_betas(domain_type *domain, int level_f, int level_c);
  void    interpolation_constant(domain_type *domain, int level_f, double prescale_f, int id_f, int id_c);
  void      interpolation_linear(domain_type *domain, int level_f, double prescale_f, int id_f, int id_c);

  void         exchange_boundary(domain_type *domain, int level, int grid_id, int exchange_faces, int exchange_edges, int exchange_corners);

double                       dot(domain_type *domain, int level, int id_a, int id_b);
double                      norm(domain_type *domain, int level, int grid_id);
  void                 add_grids(domain_type *domain, int level, int id_c, double scale_a, int id_a, double scale_b, int id_b);
  void                scale_grid(domain_type *domain, int level, int id_c, double scale_a, int id_a);
  void                 zero_grid(domain_type *domain, int level, int grid_id);
  void initialize_grid_to_scalar(domain_type *domain, int level, int grid_id, double scalar);
  void shift_grid_to_sum_to_zero(domain_type *domain, int level, int id_a);
  void                     noise(domain_type *domain, int level, int id_c, double range_low, double range_high);

  void                 mul_grids(domain_type *domain, int level, int id_c, double scale, int id_a, int id_b);
  void              matmul_grids(domain_type *domain, int level, double *C, int *id_A, int *id_B, int rows, int cols, int A_equals_B_transpose);

  void buffer_copy(double * __restrict__ destination, double * __restrict__ source, int elements, int useCacheBypass);


//------------------------------------------------------------------------------------------------------------------------------
  void __box_smooth_GSRB_multiple(box_type *box, int phi_id, int rhs_id, double a, double b, int sweep);
  void __box_smooth_GSRB_multiple_threaded(box_type *box, int phi_id, int rhs_id, double a, double b, int sweep);
  void __box_residual(box_type *box, int res_id, int phi_id, int rhs_id, double a, double b);
  void __box_residual_and_restriction(box_type *fine, int phi_id, int rhs_id, box_type *coarse, int res_id, double a, double b);
  void __box_restriction(box_type *fine, int id_f, box_type *coarse, int id_c);
  void __box_restriction_betas(box_type *fine, box_type *coarse);
  void __box_interpolation(box_type *fine, double prescale_f, int id_f, box_type *coarse, int id_c);
  void __box_grid_to_surface_bufs(box_type *box, int grid_id);
  void __box_ghost_bufs_to_grid(box_type *box, int grid_id);
  void __box_zero_grid(box_type *box,int grid_id);
  void __box_rebuild_lambda(box_type *box, double a, double b);
  void __box_add_grids(box_type *box, int id_c, double scale_a, int id_a, double scale_b, int id_b);
  void __box_scale_grid(box_type *box, int id_c, double scale_a, int id_a);
  void __box_initialize_grid_to_scalar(box_type *box, int grid_id, double scalar);
