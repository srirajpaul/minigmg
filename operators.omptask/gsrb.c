//------------------------------------------------------------------------------------------------------------------------------
// Samuel Williams
// SWWilliams@lbl.gov
// Lawrence Berkeley National Lab
//------------------------------------------------------------------------------------------------------------------------------
#include <stdint.h>
#include "../timer.h"
//------------------------------------------------------------------------------------------------------------------------------
#define BlockJ 16
#define BlockK 4
#define MAX(a, b)  (((a) > (b)) ? (a) : (b))
#define MIN(a, b)  (((a) < (b)) ? (a) : (b))
//------------------------------------------------------------------------------------------------------------------------------
void __box_smooth_GSRB_multiple(box_type *box, int phi_id, int rhs_id, double a, double b, int sweep){
  int jj,kk;
  int pencil = box->pencil;
  int plane = box->plane;
  int ghosts = box->ghosts;
   double h2inv = 1.0/(box->h*box->h);
   double * __restrict__ phi    = box->grids[  phi_id] + ghosts*plane + ghosts*pencil + ghosts; // i.e. [0] = first non ghost zone point
   double * __restrict__ rhs    = box->grids[  rhs_id] + ghosts*plane + ghosts*pencil + ghosts;
   double * __restrict__ alpha  = box->grids[__alpha ] + ghosts*plane + ghosts*pencil + ghosts;
   double * __restrict__ beta_i = box->grids[__beta_i] + ghosts*plane + ghosts*pencil + ghosts;
   double * __restrict__ beta_j = box->grids[__beta_j] + ghosts*plane + ghosts*pencil + ghosts;
   double * __restrict__ beta_k = box->grids[__beta_k] + ghosts*plane + ghosts*pencil + ghosts;
   double * __restrict__ lambda = box->grids[__lambda] + ghosts*plane + ghosts*pencil + ghosts;
  int color; //  0=red, 1=black
  int ghostsToOperateOn=ghosts-1;
  int s;
  int big_box=0;
  if(box->dim.k>8)big_box=1;
  if(box->dim.j>8)big_box=1;
  for(s=0,color=sweep;s<ghosts;s++,color++,ghostsToOperateOn--){

    for(kk=0-ghostsToOperateOn;kk<box->dim.k+ghostsToOperateOn;kk+=BlockK){
    for(jj=0-ghostsToOperateOn;jj<box->dim.j+ghostsToOperateOn;jj+=BlockJ){
    #pragma omp task if(big_box)
    {
    int i,j,k;
    int highK,highJ;
    highK = MIN(kk+BlockK,box->dim.k+ghostsToOperateOn);
    highJ = MIN(jj+BlockJ,box->dim.j+ghostsToOperateOn);
    for(k=kk;k<highK;k++){
    for(j=jj;j<highJ;j++){
      #if   defined(__GSRB_CONDITIONAL)
      #pragma simd always
      for(i=0-ghostsToOperateOn;i<box->dim.i+ghostsToOperateOn;i++){
            int ijk = i + j*pencil + k*plane;
            int doit = ((i^(j^k^color^1))&1);
            double helmholtz =  a*alpha[ijk]*phi[ijk]
                               -b*h2inv*(
                                  beta_i[ijk+1     ]*( phi[ijk+1     ]-phi[ijk       ] )
                                 -beta_i[ijk       ]*( phi[ijk       ]-phi[ijk-1     ] )
                                 +beta_j[ijk+pencil]*( phi[ijk+pencil]-phi[ijk       ] )
                                 -beta_j[ijk       ]*( phi[ijk       ]-phi[ijk-pencil] )
                                 +beta_k[ijk+plane ]*( phi[ijk+plane ]-phi[ijk       ] )
                                 -beta_k[ijk       ]*( phi[ijk       ]-phi[ijk-plane ] )
                                );
            //double delta = (doit) ? lambda[ijk]*(helmholtz-rhs[ijk]) : 0.0;
            //phi[ijk] = phi[ijk] - delta;
            phi[ijk] = (doit) ? phi[ijk] - lambda[ijk]*(helmholtz-rhs[ijk]) : phi[ijk];
      }
      #elif defined(__GSRB_STRIDE2)
      for(i=((j^k^color^1)&1)+1-ghosts;i<box->dim.i+ghostsToOperateOn;i+=2){ // stride-2 GSRB
            int ijk = i + j*pencil + k*plane;
            double helmholtz =  a*alpha[ijk]*phi[ijk]
                               -b*h2inv*(
                                  beta_i[ijk+1     ]*( phi[ijk+1     ]-phi[ijk       ] )
                                 -beta_i[ijk       ]*( phi[ijk       ]-phi[ijk-1     ] )
                                 +beta_j[ijk+pencil]*( phi[ijk+pencil]-phi[ijk       ] )
                                 -beta_j[ijk       ]*( phi[ijk       ]-phi[ijk-pencil] )
                                 +beta_k[ijk+plane ]*( phi[ijk+plane ]-phi[ijk       ] )
                                 -beta_k[ijk       ]*( phi[ijk       ]-phi[ijk-plane ] )
                                );
            phi[ijk] = phi[ijk] - lambda[ijk]*(helmholtz-rhs[ijk]);
      }
      #else
      for(i=0-ghostsToOperateOn;i<box->dim.i+ghostsToOperateOn;i++){
      if((i^j^k^color^1)&1){ // looks very clean when [0] is i,j,k=0,0,0 
            int ijk = i + j*pencil + k*plane;
            double helmholtz =  a*alpha[ijk]*phi[ijk]
                               -b*h2inv*(
                                  beta_i[ijk+1     ]*( phi[ijk+1     ]-phi[ijk       ] )
                                 -beta_i[ijk       ]*( phi[ijk       ]-phi[ijk-1     ] )
                                 +beta_j[ijk+pencil]*( phi[ijk+pencil]-phi[ijk       ] )
                                 -beta_j[ijk       ]*( phi[ijk       ]-phi[ijk-pencil] )
                                 +beta_k[ijk+plane ]*( phi[ijk+plane ]-phi[ijk       ] )
                                 -beta_k[ijk       ]*( phi[ijk       ]-phi[ijk-plane ] )
                                );
            phi[ijk] = phi[ijk] - lambda[ijk]*(helmholtz-rhs[ijk]);
      }}
      #endif
    }}}}}
    if(ghostsToOperateOn>0){
    #pragma omp taskwait
    }
  } // sweep
}


//------------------------------------------------------------------------------------------------------------------------------
void smooth(domain_type * domain, int level, int phi_id, int rhs_id, double a, double b, int sweep){
  uint64_t _timeStart = CycleTime();

  #pragma omp parallel
  {
    int box;
    #pragma omp for private(box) nowait // <<< needs to be omp for rather than single in order to get enough task injection.  <<< needs to be no wait to ensure idle cores can grab tasks asap
    for(box=0;box<domain->subdomains_per_rank;box++){
      //#pragma omp task // << too slow to create a omp task for a loop iteration which is itself an omp task
      __box_smooth_GSRB_multiple(&domain->subdomains[box].levels[level],phi_id,rhs_id,a,b,sweep);
    }
  }
  domain->cycles.smooth[level] += (uint64_t)(CycleTime()-_timeStart);
}


