eval 'exec perl $0 $*'
        if 0;
$NumArgs = $#ARGV+1;

$ARGV0 = $ARGV[0];
$ARGV0 =~ tr/A-Z/a-z/;

#==========================================================================================================================================
open(F,">./smooth.c");
print F "//------------------------------------------------------------------------------------------------------------------------------\n";
print F "// Samuel Williams\n";
print F "// SWWilliams\@lbl.gov\n";
print F "// Lawrence Berkeley National Lab\n";
print F "//------------------------------------------------------------------------------------------------------------------------------\n";
print F "#include <stdint.h>\n";
print F "#include \"../timer.h\"\n";
print F "//------------------------------------------------------------------------------------------------------------------------------\n";
$GlobalU = 16;
$INTERLEAVED_SMT        = 1;
$SMT_TOTAL_THREADS      = 2;
$SMT_STENCIL_THREADS    = 1; 
$SMT_SCOUT_MODE         = 2; # 0=none,  1=prefetch to warm up the L1,  2=prefetch to warm up the L2
$DRAM_SCOUT_THREADS     = 0;
#if($DRAM_SCOUT_THREADS % 2){
#  print "ERROR, DRAM_SCOUT_THREADS($DRAM_SCOUT_THREADS) must be a multiple of 2\n";
#  exit(0);
#}
if($SMT_SCOUT_MODE > 2){
  print "ERROR, SMT_SCOUT_MODE($SMT_SCOUT_MODE) must be 0, 1, or 2.\n";
  exit(0);
}
if( ($SMT_SCOUT_MODE>0) && ($SMT_STENCIL_THREADS > $SMT_TOTAL_THREADS-1) ){
  print "ERROR, SMT_STENCIL_THREADS($SMT_STENCIL_THREADS) + SMT_SCOUT_MODE($SMT_SCOUT_MODE) exceeds the maximum number of threads per core ($SMT_TOTAL_THREADS)\n";
  exit(0);
}
$scout_id = $SMT_TOTAL_THREADS-1;
&smooth_gsrb_wavefront($GlobalU,0);
&smooth_gsrb_wavefront($GlobalU,1);
print F "//==================================================================================================\n";
print F "void smooth(domain_type * domain, int level, int phi_id, int rhs_id, double a, double b, int sweep){\n";
print F "  uint64_t _timeStart = CycleTime();\n";
print F "  int CollaborativeThreadingBoxSize = 100000; // i.e. never\n";
print F "  #ifdef __COLLABORATIVE_THREADING\n";
print F "    CollaborativeThreadingBoxSize = 1 << __COLLABORATIVE_THREADING;\n";
print F "  #endif\n";
print F "  int box;\n";
print F "  if(domain->subdomains[0].levels[level].dim.i >= CollaborativeThreadingBoxSize){\n";
print F "    for(box=0;box<domain->numsubdomains;box++){__box_smooth_GSRB_multiple_threaded(&domain->subdomains[box].levels[level],phi_id,rhs_id,a,b,hLevel,sweep);}\n";
print F "  }else{\n";
print F "    #pragma omp parallel for private(box)\n";
print F "    for(box=0;box<domain->numsubdomains;box++){__box_smooth_GSRB_multiple(&domain->subdomains[box].levels[level],phi_id,rhs_id,a,b,hLevel,sweep);}\n";
print F "  }\n";
print F "  domain->cycles.smooth[level] += (uint64_t)(CycleTime()-_timeStart);\n";
print F "}\n";
print F "//==================================================================================================\n";
close(F);
 

#==========================================================================================================================================
sub smooth_gsrb_wavefront{
  local($U,$THREADED)=@_;
  $Alignment = 4; # i.e. align to multiples of four...
  $AlignmentMinus1 = $Alignment-1;
  if($THREADED){$FunctionName = "__box_smooth_GSRB_multiple_threaded";}
           else{$FunctionName = "__box_smooth_GSRB_multiple";}
  print F "void $FunctionName(box_type *box, int phi_id, int rhs_id, double a, double b, int sweep){\n";



 
  if($THREADED){
  print F "  volatile double dummy = 0.0;\n";
  print F "  #pragma omp parallel\n";
  print F "  {\n";
  print F "    int pencil = box->pencil;\n";
  print F "    int plane = box->plane;\n";
  print F "    int plane8   = plane<<3;\n";
  print F "    int pencil8  = pencil<<3;\n";
  print F "    int ghosts = box->ghosts;\n";
  print F "    int DimI = box->dim.i;\n";
  print F "    int DimJ = box->dim.j;\n";
  print F "    int DimK = box->dim.k;\n";
  print F "    double h2inv = 1.0/(box->h*box->h);\n";
  print F "    double  * __restrict__ phi          = box->grids[  phi_id] + ghosts*plane;\n";
  print F "    double  * __restrict__ rhs          = box->grids[  rhs_id] + ghosts*plane;\n";
  print F "    double  * __restrict__ alpha        = box->grids[__alpha ] + ghosts*plane;\n";
  print F "    double  * __restrict__ beta_i       = box->grids[__beta_i] + ghosts*plane;\n";
  print F "    double  * __restrict__ beta_j       = box->grids[__beta_j] + ghosts*plane;\n";
  print F "    double  * __restrict__ beta_k       = box->grids[__beta_k] + ghosts*plane;\n";
  print F "    double  * __restrict__ lambda       = box->grids[__lambda] + ghosts*plane;\n";
  print F "    uint64_t* __restrict__ RedBlackMask = box->RedBlack_64bMask;\n";
  print F "    const __m256d       a_splat4 =               _mm256_broadcast_sd(&a);\n";
  print F "    const __m256d b_h2inv_splat4 = _mm256_mul_pd(_mm256_broadcast_sd(&b),_mm256_broadcast_sd(&h2inv));\n";
  print F "    int id      = omp_get_thread_num();\n";
  print F "    int threads = omp_get_num_threads();\n";
  print F "    int DRAM_scout_threads = $DRAM_SCOUT_THREADS;\n";
  print F "    int smt_id      = id &  $scout_id;\n";
  print F "    int smt_leader  = id & ~$scout_id;\n";
  print F "    int global_ij_start[8];\n";
  print F "    int global_ij_end[8];\n";
  print F "    int ij_start[8];\n";
  print F "    int ij_end[8];\n";
  print F "    int planeInWavefront;for(planeInWavefront=0;planeInWavefront<ghosts;planeInWavefront++){\n";
  # ERROR, this will cause a data hazard...
  #print F "      global_ij_start[planeInWavefront] = (                   (1+planeInWavefront)*pencil)&~$AlignmentMinus1;\n";
  #print F "      global_ij_end[planeInWavefront]   = ((ghosts+DimJ+ghosts-1-planeInWavefront)*pencil);\n";
  print F "      global_ij_start[planeInWavefront] = (                   (1)*pencil)&~$AlignmentMinus1;\n";
  print F "      global_ij_end[planeInWavefront]   = ((ghosts+DimJ+ghosts-1)*pencil);\n";
  # FIX, trim if first or last team...
  print F "      int TotalUnrollings = ((global_ij_end[planeInWavefront]-global_ij_start[planeInWavefront]+$U-1)/$U);\n";
  if($INTERLEAVED_SMT){
  print F "      ij_start[planeInWavefront] = global_ij_start[planeInWavefront] + $U*( (smt_leader                   )*(TotalUnrollings)/(threads-DRAM_scout_threads)) + $U*smt_id;\n";
  print F "      ij_end[planeInWavefront]   = global_ij_start[planeInWavefront] + $U*( (smt_leader+$SMT_TOTAL_THREADS)*(TotalUnrollings)/(threads-DRAM_scout_threads));\n";
  }else{
  print F "      ij_start[planeInWavefront] = global_ij_start[planeInWavefront] + $U*( (id          )*(TotalUnrollings)/(threads-DRAM_scout_threads));\n";
  print F "      ij_end[planeInWavefront]   = global_ij_start[planeInWavefront] + $U*( (id+1        )*(TotalUnrollings)/(threads-DRAM_scout_threads));\n";
  }
  if($DRAM_SCOUT_THREADS){
  print F "      if(id>=(threads-DRAM_scout_threads)){\n";
  print F "        ij_start[planeInWavefront] = -32+global_ij_start[planeInWavefront] + pencil + (plane-pencil)*(id  -(threads-DRAM_scout_threads))/DRAM_scout_threads;\n";
  print F "        ij_end[planeInWavefront]   = -32+global_ij_start[planeInWavefront] + pencil + (plane-pencil)*(id+1-(threads-DRAM_scout_threads))/DRAM_scout_threads;\n";
  print F "      }\n";
  }
  #print F "      if(id>=(threads-1-DRAM_scout_threads))ij_end[planeInWavefront] = global_ij_end[planeInWavefront];\n";
  print F "      if(ij_end[planeInWavefront]>global_ij_end[planeInWavefront])ij_end[planeInWavefront]=global_ij_end[planeInWavefront];\n"; 
  print F "    }\n";





 
  }else{ # sequential version...
 #print F "  {\n";
  print F "    int pencil = box->pencil;\n";
  print F "    int plane = box->plane;\n";
  print F "    int plane8   = plane<<3;\n";
  print F "    int pencil8  = pencil<<3;\n";
  print F "    int ghosts = box->ghosts;\n";
  print F "    int DimI = box->dim.i;\n";
  print F "    int DimJ = box->dim.j;\n";
  print F "    int DimK = box->dim.k;\n";
  print F "    double h2inv = 1.0/(box->h*box->h);\n";
  print F "    double  * __restrict__ phi          = box->grids[  phi_id] + ghosts*plane;\n";
  print F "    double  * __restrict__ rhs          = box->grids[  rhs_id] + ghosts*plane;\n";
  print F "    double  * __restrict__ alpha        = box->grids[__alpha ] + ghosts*plane;\n";
  print F "    double  * __restrict__ beta_i       = box->grids[__beta_i] + ghosts*plane;\n";
  print F "    double  * __restrict__ beta_j       = box->grids[__beta_j] + ghosts*plane;\n";
  print F "    double  * __restrict__ beta_k       = box->grids[__beta_k] + ghosts*plane;\n";
  print F "    double  * __restrict__ lambda       = box->grids[__lambda] + ghosts*plane;\n";
  print F "    uint64_t* __restrict__ RedBlackMask = box->RedBlack_64bMask;\n";
  print F "    const __m256d       a_splat4 =               _mm256_broadcast_sd(&a);\n";
  print F "    const __m256d b_h2inv_splat4 = _mm256_mul_pd(_mm256_broadcast_sd(&b),_mm256_broadcast_sd(&h2inv));\n";
  print F "    int smt_id      = 0;\n";
  print F "    int smt_leader  = 0;\n";
  print F "    int global_ij_start[8];\n";
  print F "    int global_ij_end[8];\n";
  print F "    int ij_start[8];\n";
  print F "    int ij_end[8];\n";
  print F "    int planeInWavefront;for(planeInWavefront=0;planeInWavefront<ghosts;planeInWavefront++){\n";
  print F "      global_ij_start[planeInWavefront] = (                   (1+planeInWavefront)*pencil)&~$AlignmentMinus1;\n";
  print F "      global_ij_end[planeInWavefront]   = ((ghosts+DimJ+ghosts-1-planeInWavefront)*pencil);\n";
 #print F "      global_ij_end[planeInWavefront]    = global_ij_start[planeInWavefront]+$U*((((ghosts+DimJ+ghosts-1)*pencil-1-planeInWavefront)-global_ij_start[planeInWavefront]+$U-1)/$U);\n";
  print F "      ij_start[planeInWavefront] = global_ij_start[planeInWavefront];\n";
  print F "      ij_end[planeInWavefront]   = global_ij_end[planeInWavefront];\n";
  print F "    }\n";
  }





  #if($THREADED && $DRAM_SCOUT_THREADS){
  #print F "      double * __restrict__ Prefetch_Pointers[20];\n";
  #print F "                        int Prefetch_ij_start[20];\n";
  #print F "                        int Prefetch_ij_end[  20];\n";
  #$PF_Streams=0;
  #print F "    if(id>=(threads-DRAM_scout_threads)){\n";
  #print F "      Prefetch_Pointers[$PF_Streams] =    phi-plane;Prefetch_ij_start[$PF_Streams]=pencil;Prefetch_ij_end[$PF_Streams]=plane-pencil;\n";$PF_Streams++;
  #print F "      Prefetch_Pointers[$PF_Streams] =    phi      ;Prefetch_ij_start[$PF_Streams]=     0;Prefetch_ij_end[$PF_Streams]=plane       ;\n";$PF_Streams++;
  #print F "      Prefetch_Pointers[$PF_Streams] =    phi+plane;Prefetch_ij_start[$PF_Streams]=     0;Prefetch_ij_end[$PF_Streams]=plane       ;\n";$PF_Streams++;
  #print F "      Prefetch_Pointers[$PF_Streams] = beta_k+plane;Prefetch_ij_start[$PF_Streams]=pencil;Prefetch_ij_end[$PF_Streams]=plane-pencil;\n";$PF_Streams++;
  #print F "      Prefetch_Pointers[$PF_Streams] = beta_k      ;Prefetch_ij_start[$PF_Streams]=pencil;Prefetch_ij_end[$PF_Streams]=plane-pencil;\n";$PF_Streams++;
  #print F "      Prefetch_Pointers[$PF_Streams] = beta_j      ;Prefetch_ij_start[$PF_Streams]=pencil;Prefetch_ij_end[$PF_Streams]=plane       ;\n";$PF_Streams++;
  #print F "      Prefetch_Pointers[$PF_Streams] = beta_i      ;Prefetch_ij_start[$PF_Streams]=pencil;Prefetch_ij_end[$PF_Streams]=plane       ;\n";$PF_Streams++;
  #print F "      Prefetch_Pointers[$PF_Streams] =  alpha      ;Prefetch_ij_start[$PF_Streams]=pencil;Prefetch_ij_end[$PF_Streams]=plane-pencil;\n";$PF_Streams++;
  #print F "      Prefetch_Pointers[$PF_Streams] =    rhs      ;Prefetch_ij_start[$PF_Streams]=pencil;Prefetch_ij_end[$PF_Streams]=plane-pencil;\n";$PF_Streams++;
  #print F "      Prefetch_Pointers[$PF_Streams] = lambda      ;Prefetch_ij_start[$PF_Streams]=pencil;Prefetch_ij_end[$PF_Streams]=plane-pencil;\n";$PF_Streams++;
  #print F "    }\n";
  #}

  if($THREADED && $INTERLEAVED_SMT && ($SMT_SCOUT_MODE==2)){
  $PF_Streams=0;
  print F "      double * __restrict__ Prefetch_Pointers[20];\n";
  print F "    if( (smt_id==$scout_id) ){\n";
  print F "      Prefetch_Pointers[$PF_Streams] =    phi+plane-pencil;\n";$PF_Streams++;
  print F "      Prefetch_Pointers[$PF_Streams] = beta_k+plane       ;\n";$PF_Streams++;
  print F "      Prefetch_Pointers[$PF_Streams] = beta_j             ;\n";$PF_Streams++;
  print F "      Prefetch_Pointers[$PF_Streams] = beta_i             ;\n";$PF_Streams++;
  print F "      Prefetch_Pointers[$PF_Streams] =  alpha             ;\n";$PF_Streams++;
  print F "      Prefetch_Pointers[$PF_Streams] =    rhs             ;\n";$PF_Streams++;
  print F "      Prefetch_Pointers[$PF_Streams] = lambda             ;\n";$PF_Streams++;
  print F "      Prefetch_Pointers[$PF_Streams] =    phi      -pencil;\n";$PF_Streams++;
  print F "      Prefetch_Pointers[$PF_Streams] = beta_k             ;\n";$PF_Streams++;
  print F "      Prefetch_Pointers[$PF_Streams] =    phi-plane       ;\n";$PF_Streams++;
  print F "    }\n";
  }


  print F "    int leadingK;\n";
  print F "    int kLow  =     -(ghosts-1);\n";
  print F "    int kHigh = DimK+(ghosts-1);\n";

  print F "    for(leadingK=kLow-1;leadingK<kHigh;leadingK++){\n";

  if($THREADED){
  print F "      if(ghosts>1){\n";
  #print F "        printf(\"%03d: %04d..%04d\\n\",id,ij_start[0],ij_end[0]);\n";
  print F "        #pragma omp barrier\n";
  #print F "        exit(0);\n";
  print F "      }\n";
  }

  #if($THREADED && $DRAM_SCOUT_THREADS){
  #print F "      if(id>=(threads-DRAM_scout_threads)){\n";
  #print F "        #warning using scout cores to prefetch the next plane from DRAM\n";
  #print F "        int ij,prefetch_stream;\n";
  #print F "        int prefetch_stream_high = $PF_Streams;\n";
  #print F "        for(prefetch_stream=id-(threads-DRAM_scout_threads);prefetch_stream<prefetch_stream_high;prefetch_stream+=DRAM_scout_threads){\n";
  #print F "          double * _base = Prefetch_Pointers[prefetch_stream] + (leadingK+1)*plane;\n";
  #print F "          for(ij=Prefetch_ij_start[prefetch_stream];ij<Prefetch_ij_end[prefetch_stream];ij+=32){ // prefetch...\n";
  #print F "            _mm_prefetch((const char*)(_base+ij+ 0),_MM_HINT_T2);\n";
  #print F "            _mm_prefetch((const char*)(_base+ij+ 8),_MM_HINT_T2);\n";
  #print F "            _mm_prefetch((const char*)(_base+ij+16),_MM_HINT_T2);\n";
  #print F "            _mm_prefetch((const char*)(_base+ij+24),_MM_HINT_T2);\n";
  #print F "      }}}else\n";
  #}
  if($THREADED && $DRAM_SCOUT_THREADS){
  print F "      if(id>=(threads-DRAM_scout_threads)){\n";
  print F "        #warning using scout cores to prefetch the next plane from DRAM\n";
  #print F "        int k=(leadingK+2);\n";
  #print F "        int ij        = ij_start[0]        ;\n";
  #print F "        int ijk_start = ij_start[0]+k*plane;\n";
  #print F "        int ijk       = ijk_start;\n";
  #print F "        int _ij_end   = ij_end[0];\n";
  #print F "        while(ij<_ij_end){ // prefetch a vector...\n";
  print F "        int ij,kplane=(leadingK+2)*plane;\n";
  print F "        int _ij_start    = ij_start[0];\n";
  print F "        int _ij_end      = ij_end[0];\n";
  print F "        for(ij=_ij_start;ij<_ij_end;ij+=8){\n";
  print F "          int ijk=ij+kplane;\n";
  print F "            _mm_prefetch((const char*)(   phi+ijk+ plane-pencil),_MM_HINT_T2);\n";
  print F "            _mm_prefetch((const char*)(beta_k+ijk+ plane       ),_MM_HINT_T2);\n";
  print F "            _mm_prefetch((const char*)(beta_j+ijk              ),_MM_HINT_T2);\n";
  print F "            _mm_prefetch((const char*)(beta_i+ijk              ),_MM_HINT_T2);\n";
  print F "            _mm_prefetch((const char*)( alpha+ijk              ),_MM_HINT_T2);\n";
  print F "            _mm_prefetch((const char*)(   rhs+ijk              ),_MM_HINT_T2);\n";
  print F "            _mm_prefetch((const char*)(lambda+ijk              ),_MM_HINT_T2);\n";
  #print F "            ij +=8;\n";
  #print F "            ijk+=8;\n";
  print F "      }}else\n";
  }
  #if($THREADED && $INTERLEAVED_SMT && ($SMT_SCOUT_MODE==2)){
  #print F "      if(smt_id == $scout_id){\n";
  #print F "        #warning using SMT scout threads to prefetch the next plane from DRAM\n";
  #print F "        int ij,prefetch_stream;\n";
  #print F "        int k=leadingK+1;\n";
  #print F "        int prefetch_stream_high = $PF_Streams;\n";
  #print F "        for(prefetch_stream=0;prefetch_stream<prefetch_stream_high;prefetch_stream++){\n";
  #print F "          double * _base = Prefetch_Pointers[prefetch_stream] + k*plane;\n";
  #print F "          for(ij=ij_start[0];ij<ij_end[0];ij+=16){ // prefetch...\n";
  #print F "            _mm_prefetch((const char*)(_base+ij+ 0),_MM_HINT_T1);\n";
  #print F "            _mm_prefetch((const char*)(_base+ij+ 8),_MM_HINT_T1);\n";
  #print F "      }}}else\n";
  #}
  if($THREADED && $INTERLEAVED_SMT && ($SMT_SCOUT_MODE==2)){
  print F "      if(smt_id == $scout_id){\n";
  print F "        #warning using SMT scout threads to prefetch the next plane from DRAM\n";
  #print F "        int k=(leadingK+1);\n";
  #print F "        int ij        = ij_start[0]        ;\n";
  #print F "        int ijk_start = ij_start[0]+k*plane;\n";
  #print F "        int ijk       = ijk_start;\n";
  #print F "        int _ij_end   = ij_end[0];\n";
  #print F "        while(ij<_ij_end){ // prefetch a vector...\n";
  print F "        __m256d _phi   ;\n";
  print F "        __m256d _beta_k;\n";
  print F "        __m256d _beta_j;\n";
  print F "        __m256d _beta_i;\n";
  print F "        __m256d _alpha ;\n";
  print F "        __m256d _rhs   ;\n";
  print F "        __m256d _lambda;\n";
  print F "        __m256d _temp;\n";
  print F "        int ij,kplane=(leadingK+1)*plane;\n";
  print F "        int _ij_start    = ij_start[0];\n";
  print F "        int _ij_end      = ij_end[0];\n";
  #print F "        for(ij=_ij_start;ij<_ij_end;ij+=14){int ijk=ij+kplane;_phi    *=Prefetch_Pointers[0][ijk];}\n";
  #print F "        for(ij=_ij_start;ij<_ij_end;ij+=14){int ijk=ij+kplane;_beta_k *=Prefetch_Pointers[1][ijk];}\n";
  #print F "        for(ij=_ij_start;ij<_ij_end;ij+=14){int ijk=ij+kplane;_beta_j *=Prefetch_Pointers[2][ijk];}\n";
  #print F "        for(ij=_ij_start;ij<_ij_end;ij+=14){int ijk=ij+kplane;_beta_i *=Prefetch_Pointers[3][ijk];}\n";
  #print F "        for(ij=_ij_start;ij<_ij_end;ij+=14){int ijk=ij+kplane;_alpha  *=Prefetch_Pointers[4][ijk];}\n";
  #print F "        for(ij=_ij_start;ij<_ij_end;ij+=14){int ijk=ij+kplane;_rhs    *=Prefetch_Pointers[5][ijk];}\n";
  #print F "        for(ij=_ij_start;ij<_ij_end;ij+=14){int ijk=ij+kplane;_lambda *=Prefetch_Pointers[6][ijk];}\n";
  #print F "        for(ij=_ij_start;ij<_ij_end;ij+=8){int ijk=ij+kplane;_temp=_mm256_mul_pd(_temp, _mm256_castsi256_pd(_mm256_stream_load_si256((__m256i*)(Prefetch_Pointers[0]+ijk))));}\n";
  #print F "        for(ij=_ij_start;ij<_ij_end;ij+=8){int ijk=ij+kplane;_temp=_mm256_mul_pd(_temp, _mm256_castsi256_pd(_mm256_stream_load_si256((__m256i*)(Prefetch_Pointers[1]+ijk))));}\n";
  #print F "        for(ij=_ij_start;ij<_ij_end;ij+=8){int ijk=ij+kplane;_temp=_mm256_mul_pd(_temp, _mm256_castsi256_pd(_mm256_stream_load_si256((__m256i*)(Prefetch_Pointers[2]+ijk))));}\n";
  #print F "        for(ij=_ij_start;ij<_ij_end;ij+=8){int ijk=ij+kplane;_temp=_mm256_mul_pd(_temp, _mm256_castsi256_pd(_mm256_stream_load_si256((__m256i*)(Prefetch_Pointers[3]+ijk))));}\n";
  #print F "        for(ij=_ij_start;ij<_ij_end;ij+=8){int ijk=ij+kplane;_temp=_mm256_mul_pd(_temp, _mm256_castsi256_pd(_mm256_stream_load_si256((__m256i*)(Prefetch_Pointers[4]+ijk))));}\n";
  #print F "        for(ij=_ij_start;ij<_ij_end;ij+=8){int ijk=ij+kplane;_temp=_mm256_mul_pd(_temp, _mm256_castsi256_pd(_mm256_stream_load_si256((__m256i*)(Prefetch_Pointers[5]+ijk))));}\n";
  #print F "        for(ij=_ij_start;ij<_ij_end;ij+=8){int ijk=ij+kplane;_temp=_mm256_mul_pd(_temp, _mm256_castsi256_pd(_mm256_stream_load_si256((__m256i*)(Prefetch_Pointers[6]+ijk))));}\n";
  print F "        for(ij=_ij_start;ij<_ij_end;ij+=8){int ijk=ij+kplane;\n";
  print F "        _phi   =_mm256_mul_pd(_phi   ,_mm256_load_pd(Prefetch_Pointers[0]+ijk));\n";
  print F "        _beta_k=_mm256_mul_pd(_beta_k,_mm256_load_pd(Prefetch_Pointers[1]+ijk));\n";
  print F "        _beta_j=_mm256_mul_pd(_beta_j,_mm256_load_pd(Prefetch_Pointers[2]+ijk));\n";
  print F "        _beta_i=_mm256_mul_pd(_beta_i,_mm256_load_pd(Prefetch_Pointers[3]+ijk));\n";
  print F "        _alpha =_mm256_mul_pd(_alpha ,_mm256_load_pd(Prefetch_Pointers[4]+ijk));\n";
  print F "        _rhs   =_mm256_mul_pd(_rhs   ,_mm256_load_pd(Prefetch_Pointers[5]+ijk));\n";
  print F "        _lambda=_mm256_mul_pd(_lambda,_mm256_load_pd(Prefetch_Pointers[6]+ijk));\n";
  print F "        }\n";
  #print F "        for(ij=_ij_start;ij<_ij_end;ij+=8){\n";
  #print F "          int ijk=ij+kplane;\n";
  #print F "          _phi    *=    phi[ijk+plane-pencil];\n";
  #print F "          _beta_k *= beta_k[ijk+plane       ];\n";
  #print F "          _beta_j *= beta_j[ijk             ];\n";
  #print F "          _beta_i *= beta_i[ijk             ];\n";
  #print F "          _alpha  *=  alpha[ijk             ];\n";
  #print F "          _rhs    *=    rhs[ijk             ];\n";
  #print F "          _lambda *= lambda[ijk             ];\n";
  #print F "          _mm_prefetch((const char*)(   phi+ijk+plane-pencil),_MM_HINT_T2);\n";
  #print F "          _mm_prefetch((const char*)(   phi+ijk      -pencil),_MM_HINT_T2);\n";
  #print F "          _mm_prefetch((const char*)(   phi+ijk-plane       ),_MM_HINT_T2);\n";
  #print F "          _mm_prefetch((const char*)(beta_k+ijk+plane       ),_MM_HINT_T2);\n";
  #print F "          _mm_prefetch((const char*)(beta_k+ijk             ),_MM_HINT_T2);\n";
  #print F "          _mm_prefetch((const char*)(beta_j+ijk             ),_MM_HINT_T2);\n";
  #print F "          _mm_prefetch((const char*)(beta_i+ijk             ),_MM_HINT_T2);\n";
  #print F "          _mm_prefetch((const char*)( alpha+ijk             ),_MM_HINT_T2);\n";
  #print F "          _mm_prefetch((const char*)(   rhs+ijk             ),_MM_HINT_T2);\n";
  #print F "          _mm_prefetch((const char*)(lambda+ijk             ),_MM_HINT_T2);\n";
  #print F "          ij +=8;\n";
  #print F "          ijk+=8;\n";
  #print F "        }\n";
  print F "        dummy = *((double*)&_phi) + *((double*)&_beta_k) + *((double*)&_beta_j) + *((double*)&_beta_i) + *((double*)&_alpha) + *((double*)&_rhs) + *((double*)&_lambda);\n";
  print F "      }else\n";
  }
  if($THREADED && $INTERLEAVED_SMT && ($SMT_SCOUT_MODE==1)){
  print F "      if(smt_id == $scout_id){\n";
  print F "        #warning using SMT scout threads to prefetch into the L1\n";
  print F "        int j,k;\n";
  print F "        for(planeInWavefront=0;planeInWavefront<ghosts;planeInWavefront++){\n";
  print F "          k=(leadingK-planeInWavefront);if((k>=kLow)&&(k<kHigh)){\n";
  print F "          int ij        = ij_start[planeInWavefront]        ;\n";
  print F "          int ijk_start = ij_start[planeInWavefront]+k*plane;\n";
  print F "          int ijk       = ijk_start;\n";
  print F "          int _ij_end   = ij_end[planeInWavefront];\n";
  print F "          while(ij<_ij_end){ // prefetch a vector...\n";
  print F "            _mm_prefetch((const char*)(   phi+ijk       ),_MM_HINT_T0);\n";
  print F "            _mm_prefetch((const char*)(beta_i+ijk       ),_MM_HINT_T0);\n";
  print F "            _mm_prefetch((const char*)(beta_j+ijk       ),_MM_HINT_T0);\n";
  print F "            _mm_prefetch((const char*)(beta_k+ijk       ),_MM_HINT_T0);\n";
  print F "            _mm_prefetch((const char*)( alpha+ijk       ),_MM_HINT_T0);\n";
  print F "            _mm_prefetch((const char*)(   rhs+ijk       ),_MM_HINT_T0);\n";
  print F "            _mm_prefetch((const char*)(lambda+ijk       ),_MM_HINT_T0);\n";
  print F "            _mm_prefetch((const char*)(   phi+ijk-pencil),_MM_HINT_T0);\n";
  print F "            _mm_prefetch((const char*)(   phi+ijk- plane),_MM_HINT_T0);\n";
  print F "            _mm_prefetch((const char*)(   phi+ijk+pencil),_MM_HINT_T0);\n";
  print F "            _mm_prefetch((const char*)(beta_j+ijk+pencil),_MM_HINT_T0);\n";
  print F "            _mm_prefetch((const char*)(   phi+ijk+ plane),_MM_HINT_T0);\n";
  print F "            _mm_prefetch((const char*)(beta_k+ijk+ plane),_MM_HINT_T0);\n";
 #print F "            _mm_prefetch((const char*)(RedBlackMask+ij  ),_MM_HINT_T0);\n";
  print F "            ij +=8;\n";
  print F "            ijk+=8;\n";
  print F "      }}}}else\n";
  }

  if($THREADED && $INTERLEAVED_SMT){
  print F "      if(smt_id<$SMT_STENCIL_THREADS){\n";
  }else{
  print F "      {\n";
  }
  print F "        int j,k;\n";
  print F "        for(planeInWavefront=0;planeInWavefront<ghosts;planeInWavefront++){\n";
  print F "          k=(leadingK-planeInWavefront);if((k>=kLow)&&(k<kHigh)){\n";
  $GlobalS=$GlobalU;
  if($THREADED && $INTERLEAVED_SMT){$GlobalS=$SMT_STENCIL_THREADS*$GlobalU;}
                     &smooth_gsrb_VL_avx($GlobalU,$GlobalS);
  print F "        }}\n";
  print F "      } // if stencil\n";

  print F "    } // leadingK\n";
  if($THREADED){
  print F "  } // omp parallel region\n";
  }
  print F "}\n\n\n";
}


#==========================================================================================================================================
sub smooth_gsrb_VL_avx{
  local($U,$S)=@_;

                        print  F "        uint64_t invertMask = 0-((k^planeInWavefront^sweep^1)&0x1);\n";
                        print  F "        const __m256d    invertMask4 =               _mm256_broadcast_sd((double*)&invertMask);\n";
                        #print  F "        const __m256d       a_splat4 =               _mm256_broadcast_sd(&a);\n";
                        #print  F "        const __m256d b_h2inv_splat4 = _mm256_mul_pd(_mm256_broadcast_sd(&b),_mm256_broadcast_sd(&h2inv));\n";
                        #print  F "        int ij           = ij_start[planeInWavefront];\n";
                        #print  F "        int ijk_start    = ij_start[planeInWavefront]+k*plane;\n";
                        #print  F "        int ijk          = ijk_start;\n";
                        #print  F "        int _ij_end      = ij_end[planeInWavefront];\n";
                        #print  F "        while(ij<_ij_end){ // smooth an interleaved vector...\n";

                        print  F "        int ij,kplane=k*plane;\n";
                        print  F "        int _ij_start    = ij_start[planeInWavefront];\n";
                        print  F "        int _ij_end      = ij_end[planeInWavefront];\n";
                        print  F "        for(ij=_ij_start;ij<_ij_end;ij+=$S){ // smooth a vector...\n";
                        print  F "          int ijk=ij+kplane;\n";


  for($x=0;$x<$U;$x+=4){printf(F "                __m256d helmholtz_%02d;\n",$x);}


                              printf(F "          #if 1 // this version performs alligned accesses for phi+/-1, but not betai+1 or phi+/-pencil\n",$x);
  for($x=0;$x<$U+8;$x+=4){ # pipelined loads/shuffles of phi
    if(($x>=   0)&&($x<$U+2)){printf(F "          const __m128d      temp_%02d = _mm_load_pd(phi+ijk+%3d);\n",$x,$x-2);}
    if(($x>=   0)&&($x<$U+2)){printf(F "          const __m128d      temp_%02d = _mm_load_pd(phi+ijk+%3d);\n",$x+2,$x);}
    if(($x>=   4)&&($x<$U+2)){printf(F "          const __m128d      temp_%02d = _mm_shuffle_pd(temp_%02d,temp_%02d,1);\n",$x-1,$x-2,$x+0);}
    if(($x>=   0)&&($x<$U+2)){printf(F "          const __m128d      temp_%02d = _mm_shuffle_pd(temp_%02d,temp_%02d,1);\n",$x+1,$x+0,$x+2);}
    if(($x==   4)           ){
                        printf(F "          const __m256d       phi_%02d = _mm256_insertf128_pd(_mm256_castpd128_pd256(temp_%02d),temp_%02d,1);\n",$x-4,$x-2,$x+0);
                        printf(F "          const __m256d       phi_%02s = _mm256_insertf128_pd(_mm256_castpd128_pd256(temp_%02d),temp_%02d,1);\n","m1",$x-3,$x-1);
                        printf(F "          const __m256d       phi_%02d = _mm256_insertf128_pd(_mm256_castpd128_pd256(temp_%02d),temp_%02d,1);\n",$x-3,$x-1,$x+1);}
    if(($x>=   8)&&($x<$U+4)){
                        printf(F "          const __m256d       phi_%02d = _mm256_insertf128_pd(_mm256_castpd128_pd256(temp_%02d),temp_%02d,1);\n",$x-4,$x-2,$x+0);
                        printf(F "          const __m256d       phi_%02d = _mm256_insertf128_pd(_mm256_castpd128_pd256(temp_%02d),temp_%02d,1);\n",$x-5,$x-3,$x-1);
                        printf(F "          const __m256d       phi_%02d = _mm256_insertf128_pd(_mm256_castpd128_pd256(temp_%02d),temp_%02d,1);\n",$x-3,$x-1,$x+1);}
  }
  for($x=0;$x<$U;$x+=4){printf(F "                        helmholtz_%02d =                              _mm256_mul_pd(_mm256_sub_pd(phi_%02d,phi_%02d),_mm256_loadu_pd(beta_i+ijk+       %2d)); \n",$x,$x+1,$x,$x+1);}
                   $x=0;printf(F "                        helmholtz_%02d = _mm256_sub_pd(helmholtz_%02d,_mm256_mul_pd(_mm256_sub_pd(phi_%02d,phi_%02s),_mm256_load_pd( beta_i+ijk+       %2d)));\n",$x,$x,$x,"m1",$x);
  for($x=4;$x<$U;$x+=4){printf(F "                        helmholtz_%02d = _mm256_sub_pd(helmholtz_%02d,_mm256_mul_pd(_mm256_sub_pd(phi_%02d,phi_%02d),_mm256_load_pd( beta_i+ijk+       %2d)));\n",$x,$x,$x,$x-1,$x);}
                        printf(F "                        //careful... assumes the compiler maps _mm256_load_pd to unaligned vmovupd and not the aligned version (should be faster when pencil is a multiple of 4 doubles (32 bytes)\n");
  for($x=0;$x<$U;$x+=4){printf(F "                        helmholtz_%02d = _mm256_add_pd(helmholtz_%02d,_mm256_mul_pd(_mm256_sub_pd(_mm256_load_pd( phi+ijk+pencil+%2d),                phi_%02d           ),_mm256_load_pd( beta_j+ijk+pencil+%2d)));\n",$x,$x,$x,$x,$x);}
  for($x=0;$x<$U;$x+=4){printf(F "                        helmholtz_%02d = _mm256_sub_pd(helmholtz_%02d,_mm256_mul_pd(_mm256_sub_pd(                phi_%02d           ,_mm256_load_pd( phi+ijk-pencil+%2d)),_mm256_load_pd( beta_j+ijk+       %2d)));\n",$x,$x,$x,$x,$x);}
  for($x=0;$x<$U;$x+=4){printf(F "                        helmholtz_%02d = _mm256_add_pd(helmholtz_%02d,_mm256_mul_pd(_mm256_sub_pd(_mm256_load_pd( phi+ijk+ plane+%2d),                phi_%02d           ),_mm256_load_pd( beta_k+ijk+ plane+%2d)));\n",$x,$x,$x,$x,$x);}
  for($x=0;$x<$U;$x+=4){printf(F "                        helmholtz_%02d = _mm256_sub_pd(helmholtz_%02d,_mm256_mul_pd(_mm256_sub_pd(                phi_%02d           ,_mm256_load_pd( phi+ijk- plane+%2d)),_mm256_load_pd( beta_k+ijk       +%2d)));\n",$x,$x,$x,$x,$x);}

                        printf(F "          #else // this version performs unalligned accesses for phi+/-1, betai+1 and phi+/-pencil\n",$x);
  for($x=0;$x<$U;$x+=4){printf(F "          const __m256d       phi_%02d = _mm256_load_pd(phi+ijk+%3d);\n",$x,$x);}
                        printf(F "                        //careful... assumes the compiler maps _mm256_load_pd to unaligned vmovupd and not the aligned version (should be faster when pencil is a multiple of 4 doubles (32 bytes)\n");
  for($x=0;$x<$U;$x+=4){printf(F "                        helmholtz_%02d =                              _mm256_mul_pd(_mm256_sub_pd(_mm256_load_pd(phi+ijk+       %2d),                phi_%02d           ),_mm256_load_pd(beta_i+ijk+       %2d)); \n",$x,$x+1,$x,$x+1);}
  for($x=0;$x<$U;$x+=4){printf(F "                        helmholtz_%02d = _mm256_sub_pd(helmholtz_%02d,_mm256_mul_pd(_mm256_sub_pd(                phi_%02d           ,_mm256_load_pd(phi+ijk+       %2d)),_mm256_load_pd( beta_i+ijk+       %2d)));\n",$x,$x,$x,$x-1,$x);}
  for($x=0;$x<$U;$x+=4){printf(F "                        helmholtz_%02d = _mm256_add_pd(helmholtz_%02d,_mm256_mul_pd(_mm256_sub_pd(_mm256_loadu_pd( phi+ijk+pencil+%2d),                phi_%02d           ),_mm256_loadu_pd( beta_j+ijk+pencil+%2d)));\n",$x,$x,$x,$x,$x);}
  for($x=0;$x<$U;$x+=4){printf(F "                        helmholtz_%02d = _mm256_sub_pd(helmholtz_%02d,_mm256_mul_pd(_mm256_sub_pd(                phi_%02d           ,_mm256_loadu_pd( phi+ijk-pencil+%2d)),_mm256_load_pd( beta_j+ijk+       %2d)));\n",$x,$x,$x,$x,$x);}
  for($x=0;$x<$U;$x+=4){printf(F "                        helmholtz_%02d = _mm256_add_pd(helmholtz_%02d,_mm256_mul_pd(_mm256_sub_pd(_mm256_load_pd( phi+ijk+ plane+%2d),                phi_%02d           ),_mm256_load_pd( beta_k+ijk+ plane+%2d)));\n",$x,$x,$x,$x,$x);}
  for($x=0;$x<$U;$x+=4){printf(F "                        helmholtz_%02d = _mm256_sub_pd(helmholtz_%02d,_mm256_mul_pd(_mm256_sub_pd(                phi_%02d           ,_mm256_load_pd( phi+ijk- plane+%2d)),_mm256_load_pd( beta_k+ijk       +%2d)));\n",$x,$x,$x,$x,$x);}

                        printf(F "          #endif\n",$x);
  for($x=0;$x<$U;$x+=4){printf(F "                        helmholtz_%02d = _mm256_mul_pd(helmholtz_%02d,b_h2inv_splat4);\n",$x,$x);}
  for($x=0;$x<$U;$x+=4){printf(F "                        helmholtz_%02d = _mm256_sub_pd(_mm256_mul_pd(_mm256_mul_pd(a_splat4,_mm256_load_pd(alpha+ijk+%2d)),phi_%02d),helmholtz_%02d);\n",$x,$x,$x,$x);}
  for($x=0;$x<$U;$x+=4){printf(F "                __m256d       new_%02d = _mm256_mul_pd(_mm256_load_pd(lambda+ijk+%2d),_mm256_sub_pd(helmholtz_%02d,_mm256_load_pd(rhs+ijk+%2d)));\n",$x,$x,$x,$x);}
  for($x=0;$x<$U;$x+=4){printf(F "                              new_%02d = _mm256_sub_pd(phi_%02d,new_%02d);\n",$x,$x,$x);}
  for($x=0;$x<$U;$x+=4){printf(F "           const __m256d RedBlack_%02d = _mm256_xor_pd(invertMask4,_mm256_castsi256_pd(_mm256_load_si256( (__m256i*)(RedBlackMask+ij+%2d) )));\n",$x,$x);}
 #for($x=0;$x<$U;$x+=4){printf(F "           const __m256d RedBlack_%02d = _mm256_xor_pd(invertMask4,_mm256_load_pd( (double*)(RedBlackMask+ij+%2d) ));\n",$x,$x);}
  for($x=0;$x<$U;$x+=4){printf(F "                             new_%02d  = _mm256_blendv_pd(phi_%02d,new_%02d,RedBlack_%02d);\n",$x,$x,$x,$x);}
  for($x=0;$x<$U;$x+=4){printf(F "                                         _mm256_store_pd(phi+ijk+%2d,new_%02d);\n",$x,$x);}

                         #print  F "          ij+=$S;ijk+=$S;\n";
                         print F "        }\n";

return;
}
#==========================================================================================================================================
